import mysql.connector
from peewee import *


class BaseModel(Model):
    class Meta:
        database = MySQLDatabase('histovec_logs', user='patrick', passwd='Mar;spich60')


class Daily(BaseModel):
    id = TextField()
    event_date = DateTimeField()
    year = IntegerField()
    month = IntegerField()
    day = IntegerField()
    hour = IntegerField()

    buyer = IntegerField()
    buyer_cached = IntegerField()
    buyer_decryptError = IntegerField()
    buyer_forbidden = IntegerField()
    buyer_invalid = IntegerField()
    buyer_ok = IntegerField()
    buyer_notFound = IntegerField()
    buyer_tooManyRequests = IntegerField()
    buyer_wait = IntegerField()

    holder = IntegerField()
    holder_cached = IntegerField()
    holder_forbidden = IntegerField()
    holder_invalid = IntegerField()
    holder_notFound = IntegerField()
    holder_ok = IntegerField()
    holder_tooManyRequests = IntegerField()
    holder_wait = IntegerField()

    administrative_status = IntegerField()
    contact = IntegerField()
    csa = IntegerField()
    csa_download = IntegerField()
    faq = IntegerField()
    feedback = IntegerField()
    history = IntegerField()
    home = IntegerField()
    kilometers = IntegerField()
    legal = IntegerField()
    search = IntegerField()
    technical_control = IntegerField()
    share = IntegerField()
    share_copy = IntegerField()
    share_mail = IntegerField()
    synthesis = IntegerField()
    vehicle = IntegerField()

    notFound = IntegerField()

    visiteurs = IntegerField()
    nb_vendeurs = IntegerField()
    nb_acheteurs = IntegerField()
    nb_appels_api = IntegerField()


class HourLy(BaseModel):
    id = TextField()
    event_date = DateTimeField()
    year = IntegerField()
    month = IntegerField()
    day = IntegerField()
    hour = IntegerField()
    visiteurs = IntegerField()
