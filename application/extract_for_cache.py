import os
import re
import gzip
import csv

with open('utac.csv', "w") as csvfile:
    csvwriter = csv.writer(csvfile)
    for year in (2021, 2022):
        for month in ('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'):
            rootdir = "../../logs_full/%s/%s/" % (year, month)
            for subdir, dirs, files in os.walk(rootdir):
                for file in files:
                    print(file)
                    if re.match('.*backend*', file):
                        filename = os.path.join(subdir, file)
                        m = re.findall(r'-(\d{10})_', filename)
                        date = m[0]fcac
                        with gzip.open(filename, "r") as f:
                            for line in f:
                                if re.match('.*ask_ct.*', str(line)) and not re.match(".*==>.*", str(line)):
                                    splitted = line.decode().strip().split(' ')
                                    info = splitted[2]
                                    if re.match(".*_.*", info):
                                        (immat, vin) = info.split('_')
                                        # print(date, immat, vin)
                                        csvwriter.writerow([date, immat, vin])
