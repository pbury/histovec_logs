import elasticsearch
import datetime


def build_query(action: tuple, start_date: str, end_date: str) -> dict:
    """
    Build the final Es query
    :param action:
    :param start_date:
    :param end_date:
    :return:
    """
    query = {
        "bool": {
            "must": [
            ]
        }
    }
    query['bool']['must'].append({"exists": {"field": "uuid"}})
    query['bool']['must'].append({
        "range": {
            "@timestamp": {
                "gte": start_date,
                "lte": end_date
            }
        }
    })
    for filter in action:
        query['bool']['must'].append(filter)
    return query


def es_iterate_all_documents(es: elasticsearch.Elasticsearch, index: str, query: dict, pagesize: int = 2000,
                             scroll_timeout: str = "1m", **kwargs):
    """
    Helper to iterate ALL values from a single index
    Yields all the documents.
    :param es:
    :param index:
    :param query:
    :param pagesize:
    :param scroll_timeout:
    :param kwargs:
    :return:
    """

    scroll_id = None
    is_first = True
    while True:
        # Scroll next
        if is_first:  # Initialize scroll
            result = es.search(index=index, scroll="1m", **kwargs, body={
                "size": pagesize,
                '_source': ["uuid", ],
                "query": query

            })
            is_first = False
        else:
            result = es.scroll(body={
                "scroll_id": scroll_id,
                "scroll": scroll_timeout

            })
        if "_scroll_id" in result:
            scroll_id = result["_scroll_id"]
            hits = result["hits"]["hits"]
            # On s'arrete quand c'est fini
            if not hits:
                es.clear_scroll(scroll_id=scroll_id)
                break
        else:
            break
        # Yield each entry
        yield from (hit['_source'] for hit in hits)


def upsert_metric_to_es(es: elasticsearch.Elasticsearch, source_index: str, str_start_date: str, str_end_date: str,
                        metrics: dict) -> None:
    """
    Upsert to maintain idempotence
    :param es:
    :param source_index:
    :param str_start_date:
    :param str_end_date:
    :param metrics:
    :return: None
    """
    query = {
        "bool": {
            "must": [
                {
                    "range": {
                        "@datetime": {
                            "gte": str_start_date,
                            "lte": str_end_date,
                            "format": "strict_date_optional_time"
                        }
                    }
                }
            ],
        }
    }
    print(query)
    # deleted = es.delete_by_query(index='histovec_metriques', doc_type='_doc', body={"query": query})
    # es.indices.refresh(index="histovec_metriques")
    # print(deleted)
    try:
        res = es.search(index="histovec_metriques", body={"query": query})
        for metric in res['hits']['hits']:
            deleted = es.delete(index='histovec_metriques', doc_type='_doc', id=metric['_id'])
    except elasticsearch.exceptions:
        pass
    print("in upsert", metrics)
    res = es.index("histovec_metriques", body=metrics)
    print(res)

def clear_all(es: elasticsearch.Elasticsearch) -> None:
    """
    Upsert to maintain idempotence
    :param es:
    :param source_index:
    :param str_start_date:
    :param str_end_date:
    :param metrics:
    :return: None
    """
    query = {
        "match_all": {}
    }

    try:
        res = es.search(index="histovec_metriques", body={"query": query})
        for metric in res['hits']['hits']:
            es.delete(index='histovec_metriques', doc_type='_doc', id=metric['_id'])
    except elasticsearch.exceptions:
        pass


def metrics_post_process(metrics: dict) -> dict:
    """
    compute some deduced metrics
    :param metrics:
    :return:
    """
    if metrics['buyer/ok'] != 0:
        metrics['ratio_holder_buyer'] = metrics['holder/ok'] / metrics['buyer/ok']
    metrics['fixe_buyer/ok'] = metrics['buyer/ok'] - metrics['mobile_buyer/ok']
    metrics['fixe_holder/ok'] = metrics['holder/ok'] - metrics['mobile_holder/ok']
    if metrics['mobile_holder/ok'] != 0:
        metrics['ratio_fixe_holder/ok'] = metrics['fixe_holder/ok'] / metrics['holder/ok']
    if metrics['mobile_buyer/ok'] != 0:
        metrics['ratio_fixe_buyer/ok'] = metrics['fixe_buyer/ok'] / metrics['buyer/ok']
    metrics['buyer'] = metrics['buyer/ok'] + metrics['buyer/notFound']
    metrics['holder'] = metrics['holder/ok'] + metrics['holder/notFound']
    metrics['share'] = metrics['share/copy'] + metrics['share/mail']
    metrics['api_calls'] = metrics['buyer'] + metrics['holder']
    if metrics['holder'] != 0:
        metrics['not_found_ratio'] =  metrics['holder/notFound'] / metrics['holder']
    return metrics


def get_actions_list() -> dict:
    """

    :return:
    """
    return {
        "histovec_nginx_data_*": {
            "mobile_holder/ok": (
                {
                    "match": {
                        "action.keyword": "holder/ok"
                    }
                },
                {
                    "match": {
                        "Platforme": "Mobile"
                    }
                },
            ),
            "mobile_buyer/ok": (
                {
                    "match": {
                        "action.keyword": "buyer/ok"
                    }
                },
                {
                    "match": {
                        "Platforme": "Mobile"
                    }
                },
            ),
            "holder/ok": (
                {
                    "match": {
                        "action.keyword": "holder/ok"
                    }
                },
            ),
            "holder/cached": (
                {
                    "match": {
                        "action.keyword": "holder/cached"
                    }
                },
            ),
            "holder/invalid": (
                {
                    "match": {
                        "action.keyword": "holder/invalid"
                    }
                },
            ),
            "holder/notFound": (
                {
                    "match": {
                        "action.keyword": "holder/notFound"
                    }
                },
            ),
            "buyer/ok": (
                {
                    "match": {
                        "action.keyword": "buyer/ok"
                    }
                },
            ),
            "buyer/notFound": (
                {
                    "match": {
                        "action.keyword": "buyer/notFound"
                    }
                },
            ),
            "buyer/invalid": (
                {
                    "match": {
                        "action.keyword": "buyer/invalid"
                    }
                },
            ),
            "synthesis": (
                {
                    "match": {
                        "action.keyword": "synthesis"
                    }
                },
            ),
            "vehicle": (
                {
                    "match": {
                        "action.keyword": "vehicle"
                    }
                },
            ),
            "holder": (
                {
                    "match": {
                        "action.keyword": "holder"
                    }
                },
            ),
            "administrative-status ": (
                {
                    "match": {
                        "action.keyword": "administrative-status "
                    }
                },
            ),
            "history": (
                {
                    "match": {
                        "action.keyword": "history"
                    }
                },
            ),
            "technical-control": (
                {
                    "match": {
                        "action.keyword": "technical-control"
                    }
                },
            ),
            "kilometers": (
                {
                    "match": {
                        "action.keyword": "kilometers"
                    }
                },
            ),
            "csa": (
                {
                    "match": {
                        "action.keyword": "csa"
                    }
                },
            ),
            "csa/download": (
                {
                    "match": {
                        "action.keyword": "csa/download"
                    }
                },
            ),
            "share/copy": (
                {
                    "match": {
                        "action.keyword": "share/copy"
                    }
                },
            ),
            "share/mail": (
                {
                    "match": {
                        "action.keyword": "share/mail"
                    }
                },
            ),
            "search": (
                {
                    "match": {
                        "action.keyword": "search"
                    }
                },
            ),
            "contact": (
                {
                    "match": {
                        "action.keyword": "contact"
                    }
                },
            ),
            "home": (
                {
                    "match": {
                        "action.keyword": "home"
                    }
                },
            ),
            "faq": (
                {
                    "match": {
                        "action.keyword": "faq"
                    }
                },
            ),
            "legal": (
                {
                    "match": {
                        "action.keyword": "legal"
                    }
                },
            ),
            "mail/ok": (
                {
                    "match": {
                        "action.keyword": "mail/ok"
                    }
                },
            ),
            "mail/ko": (
                {
                    "match": {
                        "action.keyword": "mail/ko"
                    }
                },
            ),
            "holder/tooManyRequests": (
                {
                    "match": {
                        "action.keyword": "holder/tooManyRequests"
                    }
                },
            ),
            "decryptError": (
                {
                    "match": {
                        "action.keyword": "decryptError"
                    }
                },
            ),
            "unavailable": (
                {
                    "match": {
                        "action.keyword": "unavailable"
                    }
                },
            ),
            "holder/forbidden": (
                {
                    "match": {
                        "action.keyword": "holder/forbidden"
                    }
                },
            ),
            "version": (
                {
                    "match": {
                        "action.keyword": "version"
                    }
                },
            ),
            "health": (
                {
                    "match": {
                        "action.keyword": "health"
                    }
                },
            ),
            "report": (
                {
                    "match": {
                        "action.keyword": "report"
                    }
                },
            ),
            "visiteurs": (
                {
                    "match_all": {}
                },
            ),
            "simplimmat/link": (
                {
                    "match": {
                        "action.keyword": "simplimmat/link"
                    }
                },
            ),
            "simplimmat/image": (
                {
                    "match": {
                        "action.keyword": "simplimmat/image"
                    }
                },
            )
        },
        "histovec_backend_data_*": {
            "ask_ct": (
                {
                    "match": {
                        "event.keyword": "ask_ct"
                    }
                },
            ),
        }
    }


def is_metric_computed(es: elasticsearch.Elasticsearch, start_datetime: datetime, metric_version: str):
    query = {
        "bool": {
            "must": [],
            "filter": [
                {
                    "match": {
                        "metric_version": metric_version
                    }
                },
                {
                    "range": {
                        "@datetime": {
                            "gte": start_datetime.strftime("%Y-%m-%d"),
                            "lte": start_datetime.strftime("%Y-%m-%d"),
                            "format": "strict_date_optional_time"
                        }
                    }
                }
            ]
        }
    }
    res = es.search(index="histovec_metriques", body={"query": query})
    return len(res['hits']['hits']) != 0


def compute_mean_per_day(es: elasticsearch.Elasticsearch, start_datetime: datetime):
    query = {
        "bool": {
            "must": [],
            "filter": [
                {
                    "range": {
                        "@datetime": {
                            "gte": start_datetime.strftime("%Y-%m-%d"),
                            "lte": start_datetime.strftime("%Y-%m-%d"),
                            "format": "strict_date_optional_time"
                        }
                    }
                }
            ]
        }
    }
    res = es.search(index="histovec_metriques", body={"query": query})
    return res['hits']['hits'][0]['_source']['visiteurs'], start_datetime.weekday()

es = elasticsearch.Elasticsearch(['http://elastic:changeme@localhost:9200'])
#     ['localhost'],
#     http_auth=('elastic', '3CKAd195+i_GmqEIpKB='),
#     port=9200,
#     scheme='http'
# )

metric_version = '1.1.3'
idx_liste = es.cat.indices(index='histovec_*', h='index').split()
# print(type(idx_liste), idx_liste)
indices = ('histovec_nginx_data_*', "histovec_backend_data_*")
actions = get_actions_list()

if 'histovec_metriques' not in idx_liste:
    es.indices.create(index='histovec_metriques')
# clear_all(es)
start_datetime = datetime.datetime(2022, 1, 1, 0, 0, 0)
last_datetime = datetime.datetime(2022, 5, 30, 23,59,59)
total_per_week_day = {
    0: 0,
    1: 0,
    2: 0,
    3: 0,
    4: 0,
    5: 0,
    6: 0
}
nb_per_week_day = {
    0: 0,
    1: 0,
    2: 0,
    3: 0,
    4: 0,
    5: 0,
    6: 0
}
while start_datetime <= last_datetime:
    end_datetime = start_datetime + datetime.timedelta(days=1) - datetime.timedelta(seconds=1)

    metrics = {
            '@datetime': start_datetime
    }

    (total, week_day) = compute_mean_per_day(es, start_datetime)
    total_per_week_day[week_day] += total
    nb_per_week_day[week_day] += 1
    start_datetime += datetime.timedelta(days=1)

for day in range(0,7):
    print (day, total_per_week_day[day]/nb_per_week_day[day])