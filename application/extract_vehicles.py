from elasticsearch import Elasticsearch
import json
import csv


# Process hits here
def process_hits(hits):
    for item in hits:
        for x in header:
            if x not in item['_source']:
                item['_source'][x] = ''
        res = [item['_source']['uuid'], item['_source']['nom_commercial'], item['_source']['date_premiere_immat'],
               item['_source']['tvv'], item['_source']['@timestamp'],
               item['_source']['marque'], item['_source']['CTEC_RLIB_CATEGORIE'],
               item['_source']['CTEC_RLIB_GENRE'], item['_source']['departement'],
               item['_source']['CTEC_RLIB_ENERGIE'], item['_source']['couleur']]
        print(res)
        writer.writerow(res)


es = Elasticsearch("http://elastic:changeme@localhost:9200")

f = open('vehicles.csv', 'w')
writer = csv.writer(f)
header = ['uuid', 'nom_commercial', 'date_premiere_immat', 'tvv', '@timestamp', 'marque', 'CTEC_RLIB_CATEGORIE',
          'CTEC_RLIB_GENRE', 'departement', 'CTEC_RLIB_ENERGIE', 'couleur']
writer.writerow(header)

# Init scroll by search
data = es.search(
    index="histovec_nginx_vehicle_*",
    scroll='2s',
    size=1000,
    query={"match_all": {}}
)

# Get the scroll ID
sid = data['_scroll_id']
scroll_size = len(data['hits']['hits'])

i = 0
while scroll_size > 0:
    i += 1
    process_hits(data['hits']['hits'])
    data = es.scroll(scroll_id=sid, scroll='2m')
    sid = data['_scroll_id']
    scroll_size = len(data['hits']['hits'])
    print(i)

f.close()
