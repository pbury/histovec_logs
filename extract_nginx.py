from datetime import datetime
import re
import gzip
from Models import Daily, HourLy
import os


def analyze_one_file(filename):
    activities = {
        'visiteurs': []
    }
    with gzip.open(filename, "r") as fh:
        for row in fh:
            row = row.decode()
            # print(row)
            m = re.search(r'\[(?P<datetime>[0-9A-Za-z/:]*)', row)
            if m:
                try:
                    timestamp = datetime.strptime(m.group('datetime'), '%d/%b/%Y:%H:%M:%S')
                except ValueError:
                    try:
                        timestamp = datetime.strptime(m.group('datetime'), '%Y-%m-%d %H:%M:%S')
                    except ValueError:
                        pass
            m = re.search(
                r'(?P<uuid>\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b)/(?P<action>[a-zA-Z\-/]*)',
                row)
            if m:
                uuid = m.group('uuid')
                action = m.group('action').replace('/', '_').replace('-', '_')
                if action not in activities:
                    activities[action] = []
                activities[action].append(uuid)
                activities['visiteurs'].append(uuid)
                activities['event_date'] = timestamp
    for action in activities:
        if action != 'event_date':
            activities[action] = len(list(set(activities[action])))

    return activities


with open('results.txt', 'w') as fw:
    root_dir = "logs"
    dirfiles = os.listdir(root_dir)
    full_paths = map(lambda name: os.path.join(root_dir, name), dirfiles)
    year_dirs = []
    for year_dir in full_paths:
        if os.path.isdir(year_dir):
            year_dirs.append(year_dir)
    for year_dir in year_dirs:
        dirfiles = os.listdir(year_dir)
        full_paths = map(lambda name: os.path.join(year_dir, name), dirfiles)
        month_dirs = []
        for month_dir in full_paths:
            if os.path.isdir(month_dir):
                month_dirs.append(month_dir)
    for month_dir in month_dirs:
        dirfiles = os.listdir(month_dir)
        full_paths = map(lambda name: os.path.join(month_dir, name), dirfiles)
        day_dirs = []
        for day_dir in full_paths:
            if os.path.isdir(day_dir):
                day_dirs.append(day_dir)
    for day_dir in day_dirs:
        dirfiles = os.listdir(day_dir)
        full_paths = map(lambda name: os.path.join(day_dir, name), dirfiles)
        for filename in full_paths:
            if os.path.isfile(filename):
                print(filename)
                m = re.search(r'.*?(?P<year>\d{4})(?P<month>\d{2})(?P<day>\d{2})(?P<hour>\d{2})', filename)
                year = int(m.group('year'))
                month = int(m.group('month'))
                day = int(m.group('day'))
                hour = int(m.group('hour'))
                file_datetime = datetime(year=year, month=month, day=day, hour=12)
                one_hour_activity = analyze_one_file(filename)
                one_hour_activity['year'] = year
                one_hour_activity['month'] = month
                one_hour_activity['day'] = day
                one_hour_activity['hour'] = hour

                hour_activity = one_hour_activity.copy()
                for key in one_hour_activity.keys():
                    if key not in ('event_date', 'year', 'month', 'day' 'hour', 'visiteurs'):
                        del hour_activity[key]
                q = HourLy.insert_many([hour_activity], fields=hour_activity.keys())
                q.execute()

                if 'day_activity' not in locals():
                    day_activity = one_hour_activity
                else:
                    for key in one_hour_activity:
                        if key not in ('event_date','year', 'month','day'):
                            if key in day_activity:
                                day_activity[key] += one_hour_activity[key]
                            else:
                                day_activity[key] = one_hour_activity[key]

        del day_activity['hour']
        day_activity['nb_vendeurs'] = day_activity['holder_ok'] + day_activity['holder_notFound']
        day_activity['nb_acheteurs'] = day_activity['buyer_ok'] + day_activity['buyer_notFound']
        day_activity['nb_appels_api'] = day_activity['buyer_ok'] + day_activity['buyer_notFound'] + day_activity['holder_ok'] + day_activity['holder_notFound']
        q = Daily.insert_many([day_activity], fields=day_activity.keys())
        q.execute()

