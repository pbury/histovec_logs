from datetime import datetime
import re
import gzip
import os
import csv


def analyze_one_file(filename, year, month, day, hour):
    activities = {
        'visiteurs': []
    }
    uuid_list = {}
    session_duration = {}
    with gzip.open(filename, "r") as fh:
        for row in fh:
            try:
                row = row.decode()
                m = re.search(r'\[(?P<datetime>[0-9A-Za-z/:]*)', row)
                if m:
                    try:
                        timestamp = datetime.strptime(m.group('datetime'), '%d/%b/%Y:%H:%M:%S')
                    except ValueError:
                        try:
                            timestamp = datetime.strptime(m.group('datetime'), '%Y-%m-%d %H:%M:%S')
                        except ValueError:
                            timestamp = datetime.now()
                m = re.search(
                    r'(?P<uuid>\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b)/(?P<action>[a-zA-Z\-/]*)',
                    row)
                if m:
                    uuid = m.group('uuid')
                    action = m.group('action').replace('/', '_').replace('-', '_').lower()
                    if action not in activities:
                        activities[action] = []
                    activities[action].append(uuid)
                    activities['visiteurs'].append(uuid)
                    if uuid in uuid_list:
                        session_duration[uuid] = (timestamp - uuid_list[uuid]).total_seconds()
                    uuid_list[uuid] = timestamp
            except UnicodeDecodeError:
                pass
        total_duration = 0
        nb = 0
        for uuid in session_duration:
            if session_duration[uuid] > 60:
                nb += 1
                total_duration += session_duration[uuid]
        if 'share_mail' in activities and 'share_copy' in activities:
            activities['share_both'] = set(activities['share_mail']) & set(activities['share_copy'])
        else:
            activities['share_both'] = []
        for action in activities:
            if action not in ('event_date', 'session_duration'):
                activities[action] = len(list(set(activities[action])))
        activities['mean_session_duration'] = total_duration / nb
        activities['event_date'] = timestamp
        activities['year'] = year
        activities['month'] = month
        activities['day'] = day
        activities['hour'] = hour

    return activities


def get_file_list():
    root_dir = "logs"
    dirfiles = os.listdir(root_dir)
    full_paths = map(lambda name: os.path.join(root_dir, name), dirfiles)
    year_dirs = []
    for year_dir in full_paths:
        if os.path.isdir(year_dir):
            year_dirs.append(year_dir)
    for year_dir in year_dirs:
        dirfiles = os.listdir(year_dir)
        full_paths = map(lambda name: os.path.join(year_dir, name), dirfiles)
        month_dirs = []
        for month_dir in full_paths:
            if os.path.isdir(month_dir):
                month_dirs.append(month_dir)
    day_dirs = []
    for month_dir in month_dirs:
        dirfiles = os.listdir(month_dir)
        full_paths = map(lambda name: os.path.join(month_dir, name), dirfiles)
        for day_dir in full_paths:
            if os.path.isdir(day_dir):
                day_dirs.append(day_dir)
    return day_dirs


csv_columns_day = ['visiteurs', 'administrative_status', 'event_date', 'holder', 'share', 'home', 'buyer_wait',
                   'synthesis', 'holder_ok', 'feedback', 'csa', 'search', 'csa_download', 'holder_notfound', 'faq',
                   'buyer_ok', 'history', 'buyer', 'buyer_invalid', 'share_copy', 'vehicle', 'holder_invalid',
                   'holder_cached', 'buyer_cached', 'share_mail', 'holder_wait', 'contact', 'buyer_notfound',
                   'holder_toomanyrequests', 'holder_forbidden', 'buyer_forbidden', 'legal', 'buyer_toomanyrequests',
                   'year', 'month', 'day', 'notfound', 'buyer_decrypterror', 'nb_vendeurs', 'nb_acheteurs',
                   'nb_appels_api', 'kilometers', 'technical_control', 'holder_unavailable', 'mail_ok', 'mail_ko',
                   'share_both', 'mean_session_duration']
csv_columns_hour = csv_columns_day
with open('hourly.txt', 'w') as hour_fh:
    hourly_writer = csv.DictWriter(hour_fh, fieldnames=csv_columns_hour)
    hourly_writer.writeheader()
    with open('daily.txt', 'w') as day_fh:
        daily_writer = csv.DictWriter(day_fh, fieldnames=csv_columns_day)
        daily_writer.writeheader()
        for day_dir in get_file_list():
            dirfiles = os.listdir(day_dir)
            full_paths = map(lambda name: os.path.join(day_dir, name), dirfiles)
            found_files = False
            for filename in full_paths:
                if os.path.isfile(filename):
                    print(filename)
                    m = re.search(r'.*?nginx.(?P<year>\d{4})(?P<month>\d{2})(?P<day>\d{2})(?P<hour>\d{2})', filename)
                    if m:
                        found_files = True
                        year = int(m.group('year'))
                        month = int(m.group('month'))
                        day = int(m.group('day'))
                        hour = int(m.group('hour'))
                        if False or (year == 2021 and month == 4 and day == 25 and hour == 12):
                            one_hour_activity = analyze_one_file(filename, year, month, day, hour)
                            hour_activity = one_hour_activity.copy()
                            for key in one_hour_activity.keys():
                                if key not in csv_columns_hour:
                                    del hour_activity[key]
                            hourly_writer.writerow(hour_activity)

                            if 'day_activity' not in locals():
                                day_activity = one_hour_activity
                            else:
                                for key in one_hour_activity:
                                    if key not in ('event_date', 'year', 'month', 'day'):
                                        if key in day_activity:
                                            day_activity[key] += one_hour_activity[key]
                                        else:
                                            day_activity[key] = one_hour_activity[key]
                                    else:
                                        day_activity[key] = one_hour_activity[key]

            if found_files:
                if year == 2021 and month == 4 and day == 25 and hour == 12:
                    day_activity['nb_vendeurs'] = day_activity['holder_ok'] + day_activity['holder_notfound']
                    day_activity['nb_acheteurs'] = day_activity['buyer_ok'] + day_activity['buyer_notfound']
                    day_activity['nb_appels_api'] = day_activity['buyer_ok'] + day_activity['buyer_notfound'] + \
                                                    day_activity['holder_ok'] + day_activity['holder_notfound']
                    day = day_activity.copy()
                    for key in day_activity.keys():
                        if key not in csv_columns_day:
                            del day[key]
                    daily_writer.writerow(day)
                    del day_activity
