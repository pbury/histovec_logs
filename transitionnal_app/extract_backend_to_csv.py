import json
from datetime import datetime
import re
import gzip
import os
import csv


def analyze_one_file(filename, utac_fh):
    call_end = []
    call_ko = []
    call_start = []
    call_cached = []
    no_call_annulation_ci = []
    no_call_api_not_activated = []
    no_call_ask_ct = []
    ask_ct = []
    simplimmat = []
    simplimmat_image = []
    simplimmat_lien = []
    with gzip.open(filename, "r") as fh:
        for row in fh:
            row = row.decode()
            re_call_end = r'(UTAC)\].(?P<uuid>[a-f\d\-]{36}).*(?P<vin>[a-zA-Z\d\-_+=]{37}).call_end (?P<duration>\d*)'
            m = re.search(re_call_end, row)
            if m:
                utac_fh.write(m.group('duration') + "\n")
                call_end.append(m.group('vin'))
            re_call_ = r'(UTAC)\].(?P<uuid>[a-f\d\-]{36}).*(?P<vin>[a-zA-Z\d\-_+=]{37}).'
            m = re.search(re_call_+'call_ko', row)
            if m:
                call_ko.append(m.group('vin'))
            m = re.search(re_call_ + 'call_start', row)
            if m:
                call_start.append(m.group('vin'))
            m = re.search(re_call_ + 'call_cached', row)
            if m:
                call_cached.append(m.group('vin'))
            m = re.search(re_call_ + 'no_call annulation_ci', row)
            if m:
                no_call_annulation_ci.append(m.group('vin'))
            m = re.search(re_call_ + 'no_call api_not_activated', row)
            if m:
                no_call_api_not_activated.append(m.group('vin'))
            m = re.search(re_call_ + 'no_call ask_ct', row)
            if m:
                no_call_ask_ct.append(m.group('vin'))
            m = re.search(re_call_ + 'ask_ct true', row)
            if m:
                ask_ct.append(m.group('vin'))
            m = re.search(r'simplimmat', row)
            if m:
                simplimmat.append('1')
            m = re.search(r'simplimmat/image', row)
            if m:
                simplimmat_image.append('1')
            m = re.search(r'simplimmat/lien', row)
            if m:
                simplimmat_lien.append('1')

    res = {
        'ask_ct': list(set(ask_ct)),
        'call_start': list(set(call_start)),
        'call_end': list(set(call_end)),
        'call_ko': list(set(call_ko)),
        'call_cached': list(set(call_cached)),
        'no_call_annulation_ci': list(set(no_call_annulation_ci)),
        'no_call_api_not_activated': list(set(no_call_api_not_activated)),
        'no_call_ask_ct': list(set(no_call_ask_ct)),
        'simplimmat': list(set(simplimmat)),
        'simplimmat_image': list(set(simplimmat_image)),
        'simplimmat_lien': list(set(simplimmat_lien)),
    }
    return res


def get_file_list():
    root_dir = "logs"
    dirfiles = os.listdir(root_dir)
    full_paths = map(lambda name: os.path.join(root_dir, name), dirfiles)
    year_dirs = []
    for year_dir in full_paths:
        if os.path.isdir(year_dir):
            year_dirs.append(year_dir)
    for year_dir in year_dirs:
        dirfiles = os.listdir(year_dir)
        full_paths = map(lambda name: os.path.join(year_dir, name), dirfiles)
        month_dirs = []
        for month_dir in full_paths:
            if os.path.isdir(month_dir):
                month_dirs.append(month_dir)
    day_dirs = []
    for month_dir in month_dirs:
        dirfiles = os.listdir(month_dir)
        full_paths = map(lambda name: os.path.join(month_dir, name), dirfiles)
        for day_dir in full_paths:
            if os.path.isdir(day_dir):
                day_dirs.append(day_dir)
    return day_dirs


csv_columns_day = ['year', 'month', 'day', 'ask_ct', 'call_start', 'call_end', 'call_ko', 'call_cached', 'no_call_annulation_ci', 'no_call_api_not_activated', 'no_call_ask_ct', 'simplimmat',
                   'simplimmat_image', 'simplimmat_lien']
utac_fh = open('utac_duration.csv', "w")
with open('daily_backend.csv', 'w') as day_fh:
    daily_writer = csv.DictWriter(day_fh, fieldnames=csv_columns_day)
    daily_writer.writeheader()
    for day_dir in get_file_list():
        found_files = False
        dirfiles = os.listdir(day_dir)
        full_paths = map(lambda name: os.path.join(day_dir, name), dirfiles)
        for filename in full_paths:
            if os.path.isfile(filename):
                print(filename)
                m = re.search(r'.*?backend.(?P<year>\d{4})(?P<month>\d{2})(?P<day>\d{2})(?P<hour>\d{2})', filename)
                if m:
                    found_files = True
                    year = int(m.group('year'))
                    month = int(m.group('month'))
                    day = int(m.group('day'))
                    hour = int(m.group('hour'))
                    one_hour_activity = analyze_one_file(filename, utac_fh)
                    one_hour_activity['year'] = year
                    one_hour_activity['month'] = month
                    one_hour_activity['day'] = day
                    if 'day_activity' not in locals():
                        day_activity = one_hour_activity
                    else:
                        for key in one_hour_activity:
                            if key not in ('year', 'month', 'day'):
                                if key in day_activity:
                                    day_activity[key].extend(one_hour_activity[key])
                                else:
                                    day_activity[key] = one_hour_activity[key]
                            else:
                                day_activity[key] = one_hour_activity[key]
        if found_files:
            for key in day_activity:
                if key not in ('year', 'month', 'day'):
                    day_activity[key] = len(day_activity[key])
            daily_writer.writerow(day_activity)
            del day_activity

utac_fh.close()